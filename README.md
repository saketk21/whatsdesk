# WhatsDesk

WhatsDesk is a **unofficial** client of whatsapp

this project only insert a web whatsapp in a electron app and add desktop notification

![Imgur](https://i.imgur.com/oWkBGZV.png)

## Licences

This project uses app icon from [Thoseicons.com](https://thoseicons.com/) under
[Creative Commons Licence v3.0](https://creativecommons.org/licenses/by/3.0/) and modified tray icon from
[EDT.im](http://edt.im) under [Creative Commons Licence v2.5](https://creativecommons.org/licenses/by/2.5/).
