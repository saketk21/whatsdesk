import { app, BrowserWindow, ipcMain, Menu, Tray, globalShortcut, shell } from 'electron';
import fs from 'fs';
import path from 'path';
import http from 'https';
import packa from '../package.json';
import semver from 'semver';
import notify from 'electron-main-notification'
let notificationsActives = [];
let injectScripts = [
    "notifications.js",
    "links.js",
    "menucontextual.js"
];
let win = null;
let appIcon = null;
app.on("ready", _ => {
    //https://zerkc.gitlab.io/whatsdesk/update.json
    http.get('https://zerkc.gitlab.io/whatsdesk/update.json', (res) => {
        res.setEncoding('utf8');
        res.on('data', (d) => {
            d= JSON.parse(d);
            if(semver.lt(packa.version,d.version)){
                notify("Update",{body:"Update avalible"},()=>{
                    shell.openExternal("https://zerkc.gitlab.io/whatsdesk/");
                });
            }
        });

    }).on('error', (e) => {

    });
    globalShortcut.register('CommandOrControl+Q', () => {})
    appIcon = new Tray(path.join(__dirname, "icon", "tray-icon-off.png"))
    win = new BrowserWindow({ show: false, icon: path.join(__dirname, "icon", "logo.png") });
    win.setMenu(null);
    win.loadURL("https://web.whatsapp.com/");
    win.on('ready-to-show', () => {
        win.show();
        for (let scriptName of injectScripts) {
            let script = fs.readFileSync(path.join(__dirname, "scripts", scriptName), "utf8");
            win.webContents.executeJavaScript(script);
        }
    });
    win.on('page-title-updated', (evt, title) => {
        evt.preventDefault()
        title = title.replace(/(\([0-9]+\) )?.*/, "$1WhatsDesk");
        win.setTitle(title);
        appIcon.setToolTip(title);
        if (/\([0-9]+\)/.test(title)) {
            showNotification();
        } else {
            destroyNotification();
        }
    })
    win.on("focus", () => {
        destroyNotification();
        notificationsActives = [];
    });
    win.on('show', () => {
        appIcon.setHighlightMode('always')
    })
    win.on('hide', () => {
        appIcon.setHighlightMode('never')
    })
    win.on('close', function (event) {
        event.preventDefault();
        win.hide();
    });

    const contextMenu = Menu.buildFromTemplate([
        {
            label: 'Show/Hide WhatsDesk', click: function () {
                win.isVisible() ? win.hide() : win.show()
            }
        },
        {
            label: 'Quit', click: function () {
                win.destroy();
                app.quit();
            }
        }
    ])
    appIcon.on('click', () => {
        win.isVisible() ? win.hide() : win.show()
      })

    // Make a change to the context menu
    contextMenu.items[1].checked = false
    appIcon.setToolTip('WhatsDesk')

    // Call this again for Linux because we modified the context menu
    appIcon.setContextMenu(contextMenu)


})

function showNotification() {
    win.flashFrame(true);
    appIcon.setImage(path.join(__dirname, "icon", "tray-icon-on.png"))
}
function destroyNotification() {
    win.flashFrame(false);
    appIcon.setImage(path.join(__dirname, "icon", "tray-icon-off.png"))
}

ipcMain.on('notifications', (event, arg) => {
    if (!win.isFocused()) {
        notificationsActives.push(arg);
        showNotification();
    }
    event.sender.send('notification:new', true);
})
ipcMain.on('notification:close', (event, arg) => {
    let index = notificationsActives.indexOf(arg);
    if (index >= 0) {
        notificationsActives.splice(index, 1);
    }
    if (notificationsActives.length == 0) {
        destroyNotification();
    }
})
ipcMain.on('notification:click', (event, arg) => {
    win.show();
    win.focus();
    event.sender.send('notification:new', true);
})
