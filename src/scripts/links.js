const $ = require('jquery');
const { shell } = require('electron');
ipcRenderer.on("notification:new", convertLinks);
function load() {
    $("[data-list-scroll-container] > div > div > div > div:NOT(.ignoreClickWD)").each(function (i, e) {
        $(e)
            .addClass("ignoreClickWD")
            .click(function () {
                convertLinks();
                $(`.copyable-area`).on("DOMSubtreeModified", convertLinks);
                $(`div.copyable-text.selectable-text[contenteditable="true"]`).contextMenu({
                    actions: [
                        {
                            name: "Copy",
                            fn: function () {
                                document.execCommand("copy");

                            },
                            condition:function(){
                                let t = '';
                                if (window.getSelection) {
                                    t = window.getSelection();
                                } else if (document.getSelection) {
                                    t = document.getSelection();
                                } else if (document.selection) {
                                    t = document.selection.createRange().text;
                                }
                                return t.type == "Range" && $(`div.copyable-text.selectable-text[contenteditable="true"]`).is(t.focusNode.parentNode);
                            }
                        },
                        {
                            name: "Cut",
                            fn: function () {
                                document.execCommand("cut");
                            },
                            condition:function(){
                                let t = '';
                                if (window.getSelection) {
                                    t = window.getSelection();
                                } else if (document.getSelection) {
                                    t = document.getSelection();
                                } else if (document.selection) {
                                    t = document.selection.createRange().text;
                                }
                                return t.type == "Range" && $(`div.copyable-text.selectable-text[contenteditable="true"]`).is(t.focusNode.parentNode);
                            }
                        },
                        {
                            name: "Paste",
                            fn: function () {
                                document.execCommand("paste");

                            }
                        }
                    ]
                });
            });

    })
}

function convertLinks() {
    $(`.copyable-area a:NOT(.ignoreClickWD)`).each(function (i, e) {
        $(e).addClass("ignoreClickWD").click(function (event) {
            event.preventDefault();
            shell.openExternal(e.href);
        })
    })
}
function checkLoad() {
    setTimeout(() => {
        if ($(`input.copyable-text.selectable-text[dir]`).length > 0) {
            load();
        } else {
            checkLoad();
        }

    }, 100);
}
checkLoad();